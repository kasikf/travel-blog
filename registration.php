<?php
require_once('config.php');
require_once(ROOT_PATH . "/vendor/autoload.php");

$latte = new Latte\Engine;
$latte->setTempDirectory('temp');

$errors = array();
$name = '';
$surname = '';
$username = '';
$email = '';
$password_1 = '';
$password_2 = '';

$nameError = '';
$surnameError = '';
$usernameError = '';
$emailError = '';
$passwordError = '';
$confirmPasswordError = '';

if (isset($_POST['register_btn'])) {
  $name = trim($_POST['name']);
  $surname = trim($_POST['surname']);
  $username = trim($_POST['username']);
  $email = trim($_POST['email']);
  $password_1 = trim($_POST['password']);
  $password_2 = trim($_POST['confirmPassword']);

  if (empty($name)) { array_push($errors, "Zadejte jméno"); $nameError = "Zadejte jméno"; }
  if (empty($surname)) { array_push($errors, "Zadejte příjmení"); $surnameError = "Zadejte příjmení"; }
  if (empty($username)) {  array_push($errors, "Zadejte uživatelské jméno"); $usernameError = "Zadejte uživatelské jméno"; }
  if (empty($email)) { array_push($errors, "Zadejte email"); $emailError = "Zadejte email"; }
  if (empty($password_1) || strlen($password_1) < 8) { array_push($errors, "Zadejte heslo dlouhé minimálně 8 znaků"); $passwordError = "Zadejte heslo"; }
  if ($password_1 != $password_2) { array_push($errors, "Hesla se neshodují"); $confirmPasswordError = "Hesla se neshodují"; }

  if (!TravelBlog\User::isUniqueUser($username, $email)) {
    array_push($errors, "Uživatel s tímto uživatelským jménem nebo emailem již existuje");
    $emailError = "Uživatel s tímto uživatelským jménem nebo emailem již existuje";
  }

  if (count($errors) == 0) {
    $password = password_hash($password_1, PASSWORD_DEFAULT);
    $fullName = $name . ' ' . $surname;

    $registeredUserId = TravelBlog\User::registerUser($username, $fullName, $email, $password);

    if ($registeredUserId) {
      $_SESSION['user'] = TravelBlog\User::getUserById($registeredUserId);
      header('Location: ' . BASE_URL . '/adminpanel.php');
    }
  }
}

$params = [
    'errors' => $errors,
    'name' => $name,
    'surname' => $surname,
    'username' => $username,
    'email' => $email,
    'nameError' => $nameError,
    'surnameError' => $surnameError,
    'usernameError' => $usernameError,
    'emailError' => $emailError,
    'passwordError' => $passwordError,
    'confirmPasswordError' => $confirmPasswordError
];

$latte->render(ROOT_PATH . '/templates/registration.latte', $params);