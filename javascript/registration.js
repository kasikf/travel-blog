const form = document.querySelector('form');
const username = document.querySelector("input[name='username']");
const email = document.querySelector("input[name='email']");
const password = document.querySelector("input[name='password']");
const confirmPassword = document.querySelector("input[name='confirmPassword']");
const submitButton = document.querySelector('button[type="submit"]');
const passwordError = document.querySelector("[data-errorfield='password']");
const confirmPasswordError = document.querySelector("[data-errorfield='confirmPassword']");

password.addEventListener('blur', (e) => {
  validatePassword();
});

confirmPassword.addEventListener('blur', (e) => {
  validateConfirmPassword();
});

function validatePassword() {
  if (password.value.length < 8) {
    password.setCustomValidity('Heslo musí mít alespoň 8 znaků');
  } else {
    password.setCustomValidity('');
  }
}

function validateConfirmPassword() {
  if (password.value !== confirmPassword.value) {
    confirmPassword.setCustomValidity('Hesla se neshodují');
  } else {
    confirmPassword.setCustomValidity('');
  }
}
