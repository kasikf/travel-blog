const hamburgerButton = document.querySelector('.hamburger-button');
const nav = document.querySelector('nav');

hamburgerButton.addEventListener('click', () => {
  nav.setAttribute('aria-expanded', nav.getAttribute('aria-expanded') === 'true' ? 'false' : 'true');
});
