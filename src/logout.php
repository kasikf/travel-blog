<?php
require_once('../config.php');
require_once(__DIR__ . '/../vendor/autoload.php');

TravelBlog\User::logoutUser();
header('Location: ' . BASE_URL . 'index.php');