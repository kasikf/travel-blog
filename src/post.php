<?php
namespace TravelBlog;
require_once(__DIR__ . '/../vendor/autoload.php');
class Post {
  private $db;
  private $id;
  private $title;
  private $content;
  private $profileImg;
  private $datePublic;
  private $author;
  private $destination;

  public function __construct($id, $title, $content, $profileImg, $datePublic, $author, $destination) {
    $this->id = $id;
    $this->title = $title;
    $this->content = $content;
    $this->profileImg = $profileImg;
    $this->datePublic = $datePublic;
    $this->author = $author;
    $this->destination = $destination;
  }

  public static function getAllPosts() {
    $dbConn = Database::getInstance()->getConnection();
    $sql = 'SELECT * FROM articles ORDER BY DatePublic DESC';
    $result = $dbConn->query($sql);
    $posts = [];

    if ($result->num_rows > 0) {
      while ($postData = $result->fetch_assoc()) {
        $postDestination = Post::getPostDestination($postData['Destination']);
        $postAuthor = Post::getPostAuthor($postData['Author']);
        $post = new Post($postData['idArticles'] ,$postData['Title'], $postData['Content'], $postData['ProfileImg'], $postData['DatePublic'], $postAuthor, $postDestination);
        $posts[] = $post;
      }
    }
    
    return $posts;
  }

  public static function getFilteredPosts($idDestination = 'all', $idUsers = 'all', $searchTerm = '', $sortAsc = false) {
    $dbConn = Database::getInstance()->getConnection();
    $bindTypes = '';
    $bindValues = [];
    $sql = 'SELECT * FROM articles WHERE 1';

    if ($idDestination !== 'all') {
        $sql .= ' AND Destination = ?';
        $bindTypes .= 'i';
        $bindValues[] = &$idDestination;
    }

    if ($idUsers !== 'all') {
        $sql .= ' AND Author = ?';
        $bindTypes .= 'i';
        $bindValues[] = &$idUsers;
    }

    if ($searchTerm !== '') {
        $sql .= ' AND (Title LIKE ? OR Content LIKE ?)';
        $bindTypes .= 'ss';
        $searchTerm = '%' . $searchTerm . '%';
        $bindValues[] = &$searchTerm;
        $bindValues[] = &$searchTerm;
    }
    
    $sql .= $sortAsc ? ' ORDER BY DatePublic ASC' : ' ORDER BY DatePublic DESC';

    $stmt = $dbConn->prepare($sql);
    if ($bindTypes !== '') {
        $stmt->bind_param($bindTypes, ...$bindValues);
    }

    $stmt->execute();
    $result = $stmt->get_result();
    $posts = [];

    while ($postData = $result->fetch_assoc()) {
        $postDestination = Post::getPostDestination($postData['Destination']);
        $postAuthor = Post::getPostAuthor($postData['Author']);
        $post = new Post($postData['idArticles'], $postData['Title'], $postData['Content'], $postData['ProfileImg'], $postData['DatePublic'], $postAuthor, $postDestination);
        $posts[] = $post;
    }

    return $posts;
}

  public static function getPost($postId) {
    $post = null;
    $dbConn = Database::getInstance()->getConnection();

    $stmt = $dbConn->prepare("SELECT * FROM articles WHERE idArticles = ?");
    $stmt->bind_param("i", $postId);
    $stmt->execute();
    $result = $stmt->get_result();
    
    if ($result->num_rows > 0) {
      $postData = $result->fetch_assoc();
      $postDestination = Post::getPostDestination($postData['Destination']);
      $postAuthor = Post::getPostAuthor($postData['Author']);
      $post = new Post($postData['idArticles'] ,$postData['Title'], $postData['Content'], $postData['ProfileImg'], $postData['DatePublic'], $postAuthor, $postDestination);
    }
    
    return $post;
  }

  public static function getPostDestination($destinationId) {
    $dbConn = Database::getInstance()->getConnection();
    $sql = 'SELECT * FROM destination WHERE idDestination = ' . $destinationId;
    $result = $dbConn->query($sql);
    $postDestination = $result->fetch_assoc();

    return $postDestination;
  }

  public static function getPostAuthor($authorId) {
    $dbConn = Database::getInstance()->getConnection();
    $sql = 'SELECT * FROM users WHERE idUsers = ' . $authorId;
    $result = $dbConn->query($sql);
    $postAuthor = $result->fetch_assoc();

    return $postAuthor;
  }

  public static function getDestinations() {
    $dbConn = Database::getInstance()->getConnection();
    $sql = 'SELECT * FROM destination';
    $result = $dbConn->query($sql);
    $destinations = [];

    if ($result->num_rows > 0) {
      while ($destinationData = $result->fetch_assoc()) {
        $destinations[] = $destinationData;
      }
    }

    return $destinations;
  }

  public static function getAuthors() {
    $dbConn = Database::getInstance()->getConnection();
    $sql = 'SELECT * FROM users';
    $result = $dbConn->query($sql);
    $authors = [];

    if ($result->num_rows > 0) {
      while ($userData = $result->fetch_assoc()) {
        $authors[] = $userData;
      }
    }

    return $authors;
  }

  public function getId() {
    return $this->id;
  }

  public function getTitle() {
    return $this->title;
  }

  public function getContent() {
    return $this->content;
  }

  public function getCleanContent() {
    $parsedown = new \Parsedown();
    $contentAsMarkdown = $parsedown->text($this->content);
    return strip_tags($contentAsMarkdown);
  }

  public function getContentAsMarkdown() {
    $parsedown = new \Parsedown();
    $contentAsMarkdown = $parsedown->text($this->content);

    $config = \HTMLPurifier_Config::createDefault();
    $config->set('HTML.Allowed', 'p,b,strong,i,em,a[href],ul,ol,li,h1,h2,h3,h4,h5,h6');
    $purifier = new \HTMLPurifier($config);

    $cleanContent = $purifier->purify($contentAsMarkdown);
    return $cleanContent;
  }

  public function getProfileImg() {
    return $this->profileImg;
  }

  public function getDatePublic() {
    return $this->datePublic;
  }

  public function getAuthor() {
    return $this->author;
  }

  public function getDestination() {
    return $this->destination;
  }
}