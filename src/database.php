<?php
namespace TravelBlog;
class Database {
    private static $database = 'travelblog';
    private static $instance = null;
    private $conn;

    private function __construct($server = 'localhost', $username = 'root', $password = '', $database = 'travelblog') {
        $this->conn = new \mysqli($server, $username, $password, $database);

        if ($this->conn->connect_error) {
            die("Connection failed: " . $this->conn->connect_error);
        }
    }

    public static function getInstance() {
        if (self::$instance == null) {
            self::$instance = new Database('localhost', 'root', '', self::$database);
        }
        return self::$instance;
    }

    public function getConnection() {
        return $this->conn;
    }

    public static function setDatabase($database) {
        self::$database = $database;
    }
}