<?php
namespace TravelBlog;
require_once(__DIR__ . '/../vendor/autoload.php');
require_once(__DIR__ . '/../config.php');

class User {
  public static function isUniqueUser($username, $email) {
    $dbConn = Database::getInstance()->getConnection();
    $sql = 'SELECT * FROM users WHERE UserName = ? OR UserEmail = ?';
    $stmt = $dbConn->prepare($sql);
    $stmt->bind_param('ss', $username, $email);
    $stmt->execute();
    $result = $stmt->get_result();

    if ($result->num_rows > 0) {
      return false;
    }
    return true;
  }

  public static function getUserById($userId) {
    $dbConn = Database::getInstance()->getConnection();
    $sql = 'SELECT * FROM users WHERE idUsers = ?';
    $stmt = $dbConn->prepare($sql);
    $stmt->bind_param('i', $userId);
    $stmt->execute();
    $result = $stmt->get_result();

    return $result->fetch_assoc();
  }

  public static function getUserByUsername($username) {
    $dbConn = Database::getInstance()->getConnection();
    $sql = 'SELECT * FROM users WHERE UserName = ?';
    $stmt = $dbConn->prepare($sql);
    $stmt->bind_param('s', $username);
    $stmt->execute();
    $result = $stmt->get_result();
    return $result->fetch_assoc();
  }

  public static function registerUser($username, $fullname, $email, $password, $role = 'delegate') {
    $dbConn = Database::getInstance()->getConnection();
    $sql = 'INSERT INTO users (UserName, User, UserEmail, Password, Role) VALUES(?, ?, ?, ?, ?)';
    $stmt = $dbConn->prepare($sql);
    $stmt->bind_param('sssss', $username, $fullname, $email, $password, $role);
    $stmt->execute();

    return $stmt->insert_id;
  }

  public static function loginUser($username, $password) {
    $dbConn = Database::getInstance()->getConnection();
    $sql = 'SELECT * FROM users WHERE UserName = ?';
    $stmt = $dbConn->prepare($sql);
    $stmt->bind_param('s', $username);
    $stmt->execute();
    $result = $stmt->get_result();
    
    if ($result->num_rows === 1) {
      $user = $result->fetch_assoc();

      if (password_verify($password, $user['Password'])) {
        $_SESSION['user'] = $user;
        return true;
      }
    }
    return false;
  }

  public static function logoutUser() {
    session_destroy();
  }

  public static function removeUser($userId) {
    $dbConn = Database::getInstance()->getConnection();
    $sql = 'DELETE FROM users WHERE idUsers = ?';
    $stmt = $dbConn->prepare($sql);
    $stmt->bind_param('i', $userId);
    $stmt->execute();
  }
}