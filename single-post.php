<?php
require_once('config.php');
require_once(ROOT_PATH . "/vendor/autoload.php");

$latte = new Latte\Engine;
$latte->setTempDirectory('temp');

if (isset($_GET['postId'])) {
    $post = TravelBlog\Post::getPost($_GET['postId']);
}


$params = [
    "post" => $post,
];

$latte->render(ROOT_PATH . '/templates/single-post.latte', $params);