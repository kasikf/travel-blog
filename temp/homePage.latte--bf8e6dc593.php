<?php

use Latte\Runtime as LR;

/** source: templates/homePage.latte */
final class Templatebf8e6dc593 extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl(BASE_URL)) /* line 6 */;
		echo 'style.css">
    <title>TravelBlog</title>
</head>
<body>
    <div class="container">
        <h1 class="text-center">Welcome to Travel Blog</h1>

        <div class="posts">
            <div class="post">
            <img class="post__profile" src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl(BASE_URL . '/' . $post['ProfileImg'])) /* line 15 */;
		echo '" alt="">
                <h2 class="post__title">';
		echo LR\Filters::escapeHtmlText($post['Title']) /* line 16 */;
		echo '</h2>
                <p class="post__content">';
		echo LR\Filters::escapeHtmlText($post['Content']) /* line 17 */;
		echo '</p>
                <p class="post__author">';
		echo LR\Filters::escapeHtmlText($post['User']) /* line 18 */;
		echo '</p>
                <p class="post__date">';
		echo LR\Filters::escapeHtmlText(($this->filters->date)($post['DatePublic'], 'j. n. Y')) /* line 19 */;
		echo '</p>
            </div>
        </div>
    </div>
</body>
</html>';
		return get_defined_vars();
	}

}
