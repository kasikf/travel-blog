<?php

use Latte\Runtime as LR;

/** source: C:\Xampp\htdocs\travel-blog/templates/single-post.latte */
final class Templatea61d31996b extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl(BASE_URL)) /* line 6 */;
		echo 'style.css">
    <title>TravelBlog</title>
</head>
<body>
    <main class="container container--small">
        <div class="post | flow">
            <p class="post__destination">';
		echo LR\Filters::escapeHtmlText($post->getDestination()['Name']) /* line 12 */;
		echo '</p>

            <h1 class="post__title">';
		echo LR\Filters::escapeHtmlText($post->getTitle()) /* line 14 */;
		echo '</h2>

            <div class="flex-group">
                <p class="post__author">';
		echo LR\Filters::escapeHtmlText($post->getAuthor()['User']) /* line 17 */;
		echo '</p>
                <p class="post__date">';
		echo LR\Filters::escapeHtmlText(($this->filters->date)($post->getDatePublic(), 'j. n. Y')) /* line 18 */;
		echo '</p>
            </div>

            <img class="post__profile" src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl(BASE_URL . '/' . $post->getProfileImg())) /* line 21 */;
		echo '" alt="">
            
            <p class="post__content">';
		echo LR\Filters::escapeHtmlText($post->getContent()) /* line 23 */;
		echo '</p>
        </div>
    </main>
</body>
</html>';
		return get_defined_vars();
	}

}
