<?php

use Latte\Runtime as LR;

/** source: C:\xampp\htdocs\travel-blog/templates/layout.latte */
final class Template15704b0892 extends Latte\Runtime\Template
{
	protected const BLOCKS = [
		['head' => 'blockHead', 'site-title' => 'blockSite_title', 'header' => 'blockHeader', 'primary-nav' => 'blockPrimary_nav', 'content' => 'blockContent', 'footer' => 'blockFooter'],
	];


	public function main(): array
	{
		extract($this->params);
		echo '<!DOCTYPE html>
<html lang="cs">
<head>
';
		if ($this->getParentName()) {
			return get_defined_vars();
		}
		$this->renderBlock('head', get_defined_vars()) /* line 4 */;
		echo '
</head>
<body>
  <header>
    <div class="container">
';
		$this->renderBlock('header', get_defined_vars()) /* line 16 */;
		echo '
    </div>
  </header>

  ';
		$this->renderBlock('content', get_defined_vars()) /* line 39 */;
		echo '

  <footer>
    <div class="container">
';
		$this->renderBlock('footer', get_defined_vars()) /* line 43 */;
		echo '
    </div>
  </footer>
</body>
</html>';
		return get_defined_vars();
	}


	/** {block head} on line 4 */
	public function blockHead(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		unset($ʟ_args);
		echo '  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl(BASE_URL)) /* line 7 */;
		echo 'style.css">
  <script src="https://kit.fontawesome.com/1bd59e9950.js" crossorigin="anonymous"></script>
  <script src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl(BASE_URL . '/javascript/navbar.js')) /* line 9 */;
		echo '" defer></script>
  <title>';
		$this->renderBlock('site-title', get_defined_vars()) /* line 10 */;
		echo '</title>
';
	}


	/** {block site-title} on line 10 */
	public function blockSite_title(array $ʟ_args): void
	{
		echo 'Site title';
	}


	/** {block header} on line 16 */
	public function blockHeader(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		unset($ʟ_args);
		echo '        <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl(BASE_URL)) /* line 17 */;
		echo '">
          <span class="logo link1">TravelBlog</span>
          <span class="logo link2">TravelBlog</span>
        </a>

        <nav aria-expanded="false">
          <ul class="primary-nav">
';
		$this->renderBlock('primary-nav', get_defined_vars()) /* line 24 */;
		echo '
          </ul>

          <button class="hamburger-button"></button>
        </nav>
';
	}


	/** {block primary-nav} on line 24 */
	public function blockPrimary_nav(array $ʟ_args): void
	{
		echo '              <li><a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl(BASE_URL . 'all-posts.php')) /* line 25 */;
		echo '">
                <span class="link1">Blog</span>
                <span class="link2">Blog</span>
              </a></li>
              <li><a class="button--fill" href="">Přihlásit se</a></li>
';
	}


	/** {block content} on line 39 */
	public function blockContent(array $ʟ_args): void
	{
		
	}


	/** {block footer} on line 43 */
	public function blockFooter(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		unset($ʟ_args);
		echo '        <div>&copy; ';
		echo LR\Filters::escapeHtmlText(($this->filters->date)(date('l'), 'Y')) /* line 44 */;
		echo '</div>
';
	}

}
