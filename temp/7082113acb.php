<?php

use Latte\Runtime as LR;

/** source: config.php */
final class Template7082113acb extends Latte\Runtime\Template
{

	public function main(): array
	{
		extract($this->params);
		echo '<?php
$servername = "localhost";
$username = "root";
$password = "";
$database = "travelblog";

$conn = new mysqli($servername, $username, $password, $database);

if (!$conn) {
  die("Error connecting to database: " . mysqli_connect_error());
}

// globalni constanty
define (\'ROOT_PATH\', realpath(dirname(__FILE__)));
define(\'BASE_URL\', \'http://localhost/travel-blog/\');
';
		return get_defined_vars();
	}

}
