<?php

use Latte\Runtime as LR;

/** source: C:\xampp\htdocs\travel-blog/templates/index.latte */
final class Template94a22cccca extends Latte\Runtime\Template
{
	protected const BLOCKS = [
		['site-title' => 'blockSite_title', 'content' => 'blockContent'],
	];


	public function main(): array
	{
		extract($this->params);
		echo "\n";
		if ($this->getParentName()) {
			return get_defined_vars();
		}
		$this->renderBlock('site-title', get_defined_vars()) /* line 3 */;
		echo '

';
		$this->renderBlock('content', get_defined_vars()) /* line 7 */;
		return get_defined_vars();
	}


	public function prepare(): void
	{
		extract($this->params);
		$this->parentName = (ROOT_PATH . '/templates/layout.latte');
		
	}


	/** {block site-title} on line 3 */
	public function blockSite_title(array $ʟ_args): void
	{
		echo 'TravelBlog
';
	}


	/** {block content} on line 7 */
	public function blockContent(array $ʟ_args): void
	{
		echo '<main class="container">
    <h1>Vítej na TravelBlog!</h1>
</main>
';
	}

}
