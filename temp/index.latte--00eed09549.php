<?php

use Latte\Runtime as LR;

/** source: templates/index.latte */
final class Template00eed09549 extends Latte\Runtime\Template
{
	public const Source = 'templates/index.latte';

	public const Blocks = [
		['site-title' => 'blockSite_title', 'content' => 'blockContent'],
	];


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo "\n";
		$this->renderBlock('site-title', get_defined_vars()) /* line 3 */;
		echo "\n";
		$this->renderBlock('content', get_defined_vars()) /* line 7 */;
	}


	public function prepare(): array
	{
		extract($this->params);

		$this->parentName = ROOT_PATH . '/templates/layouts/layout.latte';
		return get_defined_vars();
	}


	/** {block site-title} on line 3 */
	public function blockSite_title(array $ʟ_args): void
	{
		echo 'TravelBlog
';
	}


	/** {block content} on line 7 */
	public function blockContent(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<div class="index-hero">
    <div class="container">
        <div class="index-hero__description">
            <h1>Vítejte na TravelBlog!</h1>
            <p>TravelBlog je blogová stránka, kde najdete články z různých destinací po celém světě sepsané těmi nejvášnivějšími cestovateli!</p>
            <div class="flex-group">
                <a class="button button--fill" href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl(BASE_URL . 'all-posts.php')) /* line 14 */;
		echo '">Blog</a>
                <a class="button button--outline" href="login.php">';
		if (isset($_SESSION['user'])) /* line 15 */ {
			echo '<i class="fa-solid fa-gears"></i> Správa';
		} else /* line 15 */ {
			echo 'Přihlásit se';
		}
		echo '</a>
            </div>
        </div>
    </div>
</div>
';
	}
}
