<?php

use Latte\Runtime as LR;

/** source: templates/all-posts.latte */
final class Template4c0e3c8aa1 extends Latte\Runtime\Template
{
	public const Source = 'templates/all-posts.latte';

	public const Blocks = [
		['site-title' => 'blockSite_title', 'content' => 'blockContent'],
	];


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo "\n";
		$this->renderBlock('site-title', get_defined_vars()) /* line 3 */;
		echo "\n";
		$this->renderBlock('content', get_defined_vars()) /* line 7 */;
	}


	public function prepare(): array
	{
		extract($this->params);

		if (!$this->getReferringTemplate() || $this->getReferenceType() === 'extends') {
			foreach (array_intersect_key(['destination' => '22', 'author' => '31', 'post' => '55'], $this->params) as $ʟ_v => $ʟ_l) {
				trigger_error("Variable \$$ʟ_v overwritten in foreach on line $ʟ_l");
			}
		}
		$this->parentName = ROOT_PATH . '/templates/layouts/layout.latte';
		return get_defined_vars();
	}


	/** {block site-title} on line 3 */
	public function blockSite_title(array $ʟ_args): void
	{
		echo 'TravelBlog - články
';
	}


	/** {block content} on line 7 */
	public function blockContent(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<main class="container">
  <form action="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl(BASE_URL . 'all-posts.php')) /* line 9 */;
		echo '" method="get" class="filter">
      <div class="form-group sorter">
        <label for="sort-asc">
          <i class="sort-desc-icon sorter-icon fa-solid fa-arrow-down-9-1"></i>
          <i class="sort-asc-icon sorter-icon fa-solid fa-arrow-down-1-9"></i>
        </label>

        <input type="checkbox" name="sortAsc" id="sort-asc"';
		$ʟ_tmp = ['checked' => $sortAsc];
		echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 16 */;
		echo '>
      </div>

      <div class="form-group">
        <select name="idDestination" id="">
          <option value="all">Všechny destinace</option>
';
		foreach ($destinations as $destination) /* line 22 */ {
			echo '            <option value="';
			echo LR\Filters::escapeHtmlAttr($destination['idDestination']) /* line 23 */;
			echo '"';
			$ʟ_tmp = ['selected' => $destination['idDestination'] === $selectedDestination ? true : false];
			echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 23 */;
			echo '>';
			echo LR\Filters::escapeHtmlText($destination['Name']) /* line 23 */;
			echo '</option>
';

		}

		echo '        </select>
      </div>
      
      <div class="form-group">
        <select name="idUsers" id="">
          <option value="all">Všichni autoři</option>
';
		foreach ($authors as $author) /* line 31 */ {
			echo '            <option value="';
			echo LR\Filters::escapeHtmlAttr($author['idUsers']) /* line 32 */;
			echo '"';
			$ʟ_tmp = ['selected' => $author['idUsers'] === $selectedAuthor ? true : false];
			echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 32 */;
			echo '>';
			echo LR\Filters::escapeHtmlText($author['User']) /* line 32 */;
			echo '</option>
';

		}

		echo '        </select>
      </div>

    <div class="flex-group">
      <div class="form-group">
        <input type="text" placeholder="Hledaný výraz" name="searchTerm" id=""';
		$ʟ_tmp = ['value' => $searchTerm ? $searchTerm : ''];
		echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 39 */;
		echo '>
      </div>

      <div class="form-group filter__buttons">
        <button class="button" type="submit" name="submit">Filtrovat</button>
        <button class="button" type="submit" name="reset"><i class="fa-solid fa-rotate-right"></i></button>
      </div>
    </div>

      
  </form>

';
		if (count($posts) === 0) /* line 51 */ {
			echo '      <h2>Pro tyto filtry zatím neexistují žádné články</h2>
';
		} else /* line 53 */ {
			echo '      <div class="posts">
';
			foreach ($posts as $post) /* line 55 */ {
				echo '          <a class="posts__post" href="';
				echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl(BASE_URL . 'single-post.php?postId=' . $post->getId())) /* line 56 */;
				echo '">
            <div class="posts__post-img">
              <img src="';
				echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl(UPLOADED_IMAGES . $post->getProfileImg())) /* line 58 */;
				echo '" alt="">
            </div>
            <div class="posts__post-destination">';
				echo LR\Filters::escapeHtmlText($post->getDestination()['Name']) /* line 60 */;
				echo '</div>
            <div class="posts__post-title-area">
              <p class="posts__post-title">';
				echo LR\Filters::escapeHtmlText($post->getTitle()) /* line 62 */;
				echo '</p>
              <p class="posts__post-date"><i class="fa-regular fa-calendar"></i> ';
				echo LR\Filters::escapeHtmlText(($this->filters->date)($post->getDatePublic(), 'j. n. Y')) /* line 63 */;
				echo '</p>
            </div>
            <div class="flow" data-space="small">
              <p class="posts__post-content">';
				echo LR\Filters::escapeHtmlText(($this->filters->truncate)($post->getCleanContent(), 200)) /* line 66 */;
				echo '</p>
              <p class="posts__post-read">Číst více</p>
            </div>
          </a>
';

			}

			echo '      </div>
';
		}
		echo '</main>
';
	}
}
