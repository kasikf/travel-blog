<?php

use Latte\Runtime as LR;

/** source: C:\xampp\htdocs\travel-blog/templates/login.latte */
final class Template225ce9376a extends Latte\Runtime\Template
{
	public const Source = 'C:\\xampp\\htdocs\\travel-blog/templates/login.latte';

	public const Blocks = [
		['site-title' => 'blockSite_title', 'head' => 'blockHead', 'content' => 'blockContent'],
	];


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo "\n";
		$this->renderBlock('site-title', get_defined_vars()) /* line 3 */;
		echo "\n";
		$this->renderBlock('head', get_defined_vars()) /* line 7 */;
		echo "\n";
		$this->renderBlock('content', get_defined_vars()) /* line 12 */;
	}


	public function prepare(): array
	{
		extract($this->params);

		$this->parentName = ROOT_PATH . '/templates/layouts/layout-simple.latte';
		return get_defined_vars();
	}


	/** {block site-title} on line 3 */
	public function blockSite_title(array $ʟ_args): void
	{
		echo 'TravelBlog - login
';
	}


	/** {block head} on line 7 */
	public function blockHead(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		unset($ʟ_args);

		$this->renderBlockParent('head', get_defined_vars()) /* line 8 */;
		echo '  <script src="javascript/login.js" defer></script>
';
	}


	/** {block content} on line 12 */
	public function blockContent(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		unset($ʟ_args);

		echo '  <main class="login-page | container">
    <div class="login-card">
      <div class="login-card__form">
        <form action="" method="post" class="login-form">
          <h1 class="login-form__header | text-center">Přihlášení</h1>

          <div class="login-form__form-group">
            <label for="username">Uživatelské jméno</label>
            <input';
		$ʟ_tmp = ['class' => !empty($usernameError) || $passwordError == 'Neplatné uživatelské jméno nebo heslo' ? 'has-error' : null];
		echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 21 */;
		echo ' type="text" value="';
		echo LR\Filters::escapeHtmlAttr($username) /* line 21 */;
		echo '" name="username" id="username" placeholder="Uživatelské jméno" autocomplete="username" required>
            <p class="error">';
		echo LR\Filters::escapeHtmlText($usernameError) /* line 22 */;
		echo '</p>
          </div>

          <div class="login-form__form-group">
            <label for="password">Heslo</label>
            <input';
		$ʟ_tmp = ['class' => !empty($passwordError) ? 'has-error' : null];
		echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 27 */;
		echo ' type="password" name="password" id="password" autocomplete="current-password" required>
            <p class="error">';
		echo LR\Filters::escapeHtmlText($passwordError) /* line 28 */;
		echo '</p>
          </div>

          <div class="login-form__form-group">
            <button class="button--fill" type="submit" name="login_btn">Přihlásit se</button>
          </div>

          <p class="text-center">
            Nemáte účet? <a class="link" href="registration.php">Registrujte se</a>
          </p>
        </form>
      </div>

      <div class="login-card__image">
        <img src="assets/img/train.webp" alt="žena čekající na vlak">
      </div>
    </div>
  </main>
';
	}
}
