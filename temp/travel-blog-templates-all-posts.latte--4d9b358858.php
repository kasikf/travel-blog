<?php

use Latte\Runtime as LR;

/** source: C:\xampp\htdocs\travel-blog/templates/all-posts.latte */
final class Template4d9b358858 extends Latte\Runtime\Template
{
	protected const BLOCKS = [
		['site-title' => 'blockSite_title', 'content' => 'blockContent'],
	];


	public function main(): array
	{
		extract($this->params);
		echo "\n";
		if ($this->getParentName()) {
			return get_defined_vars();
		}
		$this->renderBlock('site-title', get_defined_vars()) /* line 3 */;
		echo '

';
		$this->renderBlock('content', get_defined_vars()) /* line 7 */;
		return get_defined_vars();
	}


	public function prepare(): void
	{
		extract($this->params);
		if (!$this->getReferringTemplate() || $this->getReferenceType() === "extends") {
			foreach (array_intersect_key(['destination' => '19', 'author' => '28', 'post' => '46'], $this->params) as $ʟ_v => $ʟ_l) {
				trigger_error("Variable \$$ʟ_v overwritten in foreach on line $ʟ_l");
			}
		}
		$this->parentName = (ROOT_PATH . '/templates/layout.latte');
		
	}


	/** {block site-title} on line 3 */
	public function blockSite_title(array $ʟ_args): void
	{
		echo 'TravelBlog - články
';
	}


	/** {block content} on line 7 */
	public function blockContent(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		unset($ʟ_args);
		echo '<main class="container">
  <form action="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl(BASE_URL . 'all-posts.php')) /* line 9 */;
		echo '" method="get">
    <div class="form-group">
    <label for="sort-asc">Od nejstarších</label>
      <input type="checkbox" name="sortAsc" id="sort-asc"';
		$ʟ_tmp = ['checked' => $sortAsc];
		echo LR\Filters::htmlAttributes(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp) /* line 12 */;
		echo '>
    </div>


    <div class="form-group">
      <select name="idDestination" id="">
        <option value="all">Všechny destinace</option>
';
		$iterations = 0;
		foreach ($destinations as $destination) /* line 19 */ {
			echo '          <option value="';
			echo LR\Filters::escapeHtmlAttr($destination['idDestination']) /* line 20 */;
			echo '"';
			$ʟ_tmp = ['selected' => $destination['idDestination'] === $selectedDestination ? true : false];
			echo LR\Filters::htmlAttributes(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp) /* line 20 */;
			echo '>';
			echo LR\Filters::escapeHtmlText($destination['Name']) /* line 20 */;
			echo '</option>
';
			$iterations++;
		}
		echo '      </select>
    </div>

    <div class="form-group">
      <select name="idUsers" id="">
        <option value="all">Všichni autoři</option>
';
		$iterations = 0;
		foreach ($authors as $author) /* line 28 */ {
			echo '          <option value="';
			echo LR\Filters::escapeHtmlAttr($author['idUsers']) /* line 29 */;
			echo '"';
			$ʟ_tmp = ['selected' => $author['idUsers'] === $selectedAuthor ? true : false];
			echo LR\Filters::htmlAttributes(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp) /* line 29 */;
			echo '>';
			echo LR\Filters::escapeHtmlText($author['User']) /* line 29 */;
			echo '</option>
';
			$iterations++;
		}
		echo '      </select>
    </div>

    <div class="form-group">
      <input type="text" name="searchTerm" id=""';
		$ʟ_tmp = ['value' => $searchTerm ? $searchTerm : ''];
		echo LR\Filters::htmlAttributes(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp) /* line 35 */;
		echo '>
    </div>

    <div class="form-group">
      <button type="submit">Filtrovat</button>
    </div>
  </form>

';
		if (count($posts) === 0) /* line 43 */ {
			echo '      <h2>Pro tyto filtry zatím neexistují žádné články</h2>
';
		} else /* line 45 */ {
			$iterations = 0;
			foreach ($posts as $post) /* line 46 */ {
				echo '        <div>
          <a href="';
				echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl(BASE_URL . 'single-post.php?postId=' . $post->getId())) /* line 48 */;
				echo '">';
				echo LR\Filters::escapeHtmlText($post->getTitle()) /* line 48 */;
				echo ', ';
				echo LR\Filters::escapeHtmlText($post->getDestination()['Name']) /* line 48 */;
				echo ', ';
				echo LR\Filters::escapeHtmlText($post->getAuthor()['User']) /* line 48 */;
				echo ', ';
				echo LR\Filters::escapeHtmlText($post->getDatePublic()) /* line 48 */;
				echo '</a>
        </div>
';
				$iterations++;
			}
		}
		echo '</main>
';
	}

}
