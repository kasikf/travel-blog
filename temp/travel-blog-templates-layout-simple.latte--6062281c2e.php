<?php

use Latte\Runtime as LR;

/** source: C:\xampp\htdocs\travel-blog/templates/layout-simple.latte */
final class Template6062281c2e extends Latte\Runtime\Template
{
	public const Source = 'C:\\xampp\\htdocs\\travel-blog/templates/layout-simple.latte';

	public const Blocks = [
		['head' => 'blockHead', 'site-title' => 'blockSite_title', 'content' => 'blockContent'],
	];


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="cs">
<head>
';
		$this->renderBlock('head', get_defined_vars()) /* line 4 */;
		echo '</head>
<body class="simple">
';
		$this->renderBlock('content', get_defined_vars()) /* line 13 */;
		echo '</body>
</html>';
	}


	/** {block head} on line 4 */
	public function blockHead(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		unset($ʟ_args);

		echo '  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="style.css">
  <script src="https://kit.fontawesome.com/1bd59e9950.js" crossorigin="anonymous"></script>
  <title>';
		$this->renderBlock('site-title', get_defined_vars()) /* line 9 */;
		echo '</title>
';
	}


	/** {block site-title} on line 9 */
	public function blockSite_title(array $ʟ_args): void
	{
		echo 'Site title';
	}


	/** {block content} on line 13 */
	public function blockContent(array $ʟ_args): void
	{
	}
}
