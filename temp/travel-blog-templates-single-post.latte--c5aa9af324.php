<?php

use Latte\Runtime as LR;

/** source: C:\xampp\htdocs\travel-blog/templates/single-post.latte */
final class Templatec5aa9af324 extends Latte\Runtime\Template
{
	protected const BLOCKS = [
		['site-title' => 'blockSite_title', 'content' => 'blockContent'],
	];


	public function main(): array
	{
		extract($this->params);
		echo "\n";
		if ($this->getParentName()) {
			return get_defined_vars();
		}
		$this->renderBlock('site-title', get_defined_vars()) /* line 3 */;
		echo '

';
		$this->renderBlock('content', get_defined_vars()) /* line 7 */;
		return get_defined_vars();
	}


	public function prepare(): void
	{
		extract($this->params);
		$this->parentName = (ROOT_PATH . '/templates/layout.latte');
		
	}


	/** {block site-title} on line 3 */
	public function blockSite_title(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		unset($ʟ_args);
		echo 'TravelBlog - ';
		echo LR\Filters::escapeHtmlText($post->getTitle()) /* line 4 */;
		echo "\n";
	}


	/** {block content} on line 7 */
	public function blockContent(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		unset($ʟ_args);
		echo '<main class="container container--small">
    <div class="post | flow">
        <a class="post__destination" href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl(BASE_URL . 'all-posts.php?idDestination=' . $post->getDestination()['idDestination'])) /* line 10 */;
		echo '">';
		echo LR\Filters::escapeHtmlText($post->getDestination()['Name']) /* line 10 */;
		echo '</a>
    
        <h1 class="post__title">';
		echo LR\Filters::escapeHtmlText($post->getTitle()) /* line 12 */;
		echo '</h2>
    
        <div class="flex-group">
            <a class="post__author" href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl(BASE_URL . 'all-posts.php?idUsers=' . $post->getAuthor()['idUsers'])) /* line 15 */;
		echo '"><i class="fa-regular fa-user"></i> ';
		echo LR\Filters::escapeHtmlText($post->getAuthor()['User']) /* line 15 */;
		echo '</a>
            <p class="post__date"><i class="fa-regular fa-calendar"></i> ';
		echo LR\Filters::escapeHtmlText(($this->filters->date)($post->getDatePublic(), 'j. n. Y')) /* line 16 */;
		echo '</p>
        </div>
    
        <img class="post__profile" src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl(UPLOADED_IMAGES . $post->getProfileImg())) /* line 19 */;
		echo '" alt="">
    
        <p class="post__content">';
		echo LR\Filters::escapeHtmlText($post->getContent()) /* line 21 */;
		echo '</p>
    </div>
</main>
';
	}

}
