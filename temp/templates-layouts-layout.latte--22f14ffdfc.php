<?php

use Latte\Runtime as LR;

/** source: C:\xampp\htdocs\travel-blog/templates/layouts/layout.latte */
final class Template22f14ffdfc extends Latte\Runtime\Template
{
	public const Source = 'C:\\xampp\\htdocs\\travel-blog/templates/layouts/layout.latte';

	public const Blocks = [
		['head' => 'blockHead', 'site-title' => 'blockSite_title', 'header' => 'blockHeader', 'primary-nav' => 'blockPrimary_nav', 'content' => 'blockContent', 'footer' => 'blockFooter'],
	];


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<!DOCTYPE html>
<html lang="cs">
<head>
';
		$this->renderBlock('head', get_defined_vars()) /* line 4 */;
		echo '</head>
<body>
  <header>
    <div class="container">
';
		$this->renderBlock('header', get_defined_vars()) /* line 16 */;
		echo '    </div>
  </header>

';
		$this->renderBlock('content', get_defined_vars()) /* line 42 */;
		echo '
  <footer>
      <div class="footer__main">
        <div class="container text-center">
          <div class="footer__heading">TravelBlog</div>
          <p>
            TravelBlog je blogová stránka, kde najdete články z různých destinací po celém světě. Stránka je žákovským projektem a na její tvorbě se podíleli: František&nbsp;Kasík, Lukáš&nbsp;Janda, Ernst&nbsp;Christoph&nbsp;Leschka a Bui&nbsp;Dai&nbsp;Duong.
          </p>
          <div class="footer__socials">
            <ul role="list">
              <a href=""><i class="fa-brands fa-x-twitter"></i></a>
              <a href=""><i class="fa-brands fa-facebook"></i></a>
              <a href=""><i class="fa-brands fa-instagram"></i></a>
            </ul>
          </div>
        </div>
      </div>

      <div class="footer__footer">
        <div class="container">
';
		$this->renderBlock('footer', get_defined_vars()) /* line 63 */;
		echo '
          <div>VOŠ a SPŠE Plzeň</div>
        </div>
      </div>
  </footer>
</body>
</html>';
	}


	/** {block head} on line 4 */
	public function blockHead(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		unset($ʟ_args);

		echo '  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl(BASE_URL)) /* line 7 */;
		echo 'style.css">
  <script src="https://kit.fontawesome.com/1bd59e9950.js" crossorigin="anonymous"></script>
  <script src="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl(BASE_URL . '/javascript/navbar.js')) /* line 9 */;
		echo '" defer></script>
  <title>';
		$this->renderBlock('site-title', get_defined_vars()) /* line 10 */;
		echo '</title>
';
	}


	/** {block site-title} on line 10 */
	public function blockSite_title(array $ʟ_args): void
	{
		echo 'Site title';
	}


	/** {block header} on line 16 */
	public function blockHeader(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		unset($ʟ_args);

		echo '        <a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl(BASE_URL)) /* line 17 */;
		echo '">
          <span class="logo link1">TravelBlog</span>
          <span class="logo link2">TravelBlog</span>
        </a>

        <nav aria-expanded="false">
          <ul class="primary-nav">
';
		$this->renderBlock('primary-nav', get_defined_vars()) /* line 24 */;
		echo '          </ul>

          <button class="hamburger-button"></button>
        </nav>
';
	}


	/** {block primary-nav} on line 24 */
	public function blockPrimary_nav(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		unset($ʟ_args);

		echo '              <li><a href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl(BASE_URL . 'all-posts.php')) /* line 25 */;
		echo '">
                <span class="link1">Blog</span>
                <span class="link2">Blog</span>
              </a></li>
';
		if (isset($_SESSION['user'])) /* line 29 */ {
			echo '                <li><a href="src/logout.php" class="button--outline">Odhlásit se</a></li>
';
		}
		echo '              <li><a class="button--fill" href="';
		echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl(BASE_URL . 'login.php')) /* line 32 */;
		echo '">';
		if (isset($_SESSION['user'])) /* line 32 */ {
			echo '<i class="fa-solid fa-gears"></i> Správa';
		} else /* line 32 */ {
			echo 'Přihlásit se';
		}
		echo '</a></li>
';
	}


	/** {block content} on line 42 */
	public function blockContent(array $ʟ_args): void
	{
	}


	/** {block footer} on line 63 */
	public function blockFooter(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		unset($ʟ_args);

		echo '            <div>Copyright &copy; ';
		echo LR\Filters::escapeHtmlText(($this->filters->date)(date('l'), 'Y')) /* line 64 */;
		echo '</div>
';
	}
}
