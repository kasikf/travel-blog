<?php

use Latte\Runtime as LR;

/** source: C:\xampp\htdocs\travel-blog/templates/registration.latte */
final class Template79be943807 extends Latte\Runtime\Template
{
	public const Source = 'C:\\xampp\\htdocs\\travel-blog/templates/registration.latte';

	public const Blocks = [
		['site-title' => 'blockSite_title', 'head' => 'blockHead', 'content' => 'blockContent'],
	];


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo "\n";
		$this->renderBlock('site-title', get_defined_vars()) /* line 3 */;
		echo "\n";
		$this->renderBlock('head', get_defined_vars()) /* line 7 */;
		echo "\n";
		$this->renderBlock('content', get_defined_vars()) /* line 12 */;
	}


	public function prepare(): array
	{
		extract($this->params);

		$this->parentName = ROOT_PATH . '/templates/layouts/layout-simple.latte';
		return get_defined_vars();
	}


	/** {block site-title} on line 3 */
	public function blockSite_title(array $ʟ_args): void
	{
		echo 'TravelBlog - registrace
';
	}


	/** {block head} on line 7 */
	public function blockHead(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		unset($ʟ_args);

		$this->renderBlockParent('head', get_defined_vars()) /* line 8 */;
		echo '  <script src="javascript/registration.js" defer></script>
';
	}


	/** {block content} on line 12 */
	public function blockContent(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		unset($ʟ_args);

		echo '  <main class="login-page | container">
    <div class="login-card">
      <div class="login-card__form">
        <form action="" method="post" class="login-form">
          <h1 class="login-form__header | text-center">Registrace</h1>

          <div class="login-form__form-group">
            <label for="name">Jméno</label>
            <input';
		$ʟ_tmp = ['class' => !empty($nameError) ? 'has-error' : null];
		echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 21 */;
		echo ' type="text" name="name" id="name" placeholder="Pepa" value="';
		echo LR\Filters::escapeHtmlAttr($name) /* line 21 */;
		echo '" autocomplete="given-name" required>
            <p class="error" data-errorfield="name">';
		echo LR\Filters::escapeHtmlText($nameError) /* line 22 */;
		echo '</p>
          </div>

          <div class="login-form__form-group">
            <label for="surname">Příjmení</label>
            <input';
		$ʟ_tmp = ['class' => !empty($surnameError) ? 'has-error' : null];
		echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 27 */;
		echo ' type="text" name="surname" id="surname" placeholder="Novák" value="';
		echo LR\Filters::escapeHtmlAttr($surname) /* line 27 */;
		echo '" autocomplete="family-name" required>
            <p class="error" data-errorfield="surname">';
		echo LR\Filters::escapeHtmlText($surnameError) /* line 28 */;
		echo '</p>
          </div>

          <div class="login-form__form-group">
            <label for="username">Uživatelské jméno</label>
            <input';
		$ʟ_tmp = ['class' => !empty($usernameError) || $emailError == 'Uživatel s tímto uživatelským jménem nebo emailem již existuje' ? 'has-error' : null];
		echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 33 */;
		echo ' type="text" name="username" id="username" placeholder="zabijakpepa" value="';
		echo LR\Filters::escapeHtmlAttr($username) /* line 33 */;
		echo '" autocomplete="off" required>
            <p class="error" data-errorfield="username">';
		echo LR\Filters::escapeHtmlText($usernameError) /* line 34 */;
		echo '</p>
          </div>

          <div class="login-form__form-group">
            <label for="email">Email</label>
            <input';
		$ʟ_tmp = ['class' => !empty($emailError) ? 'has-error' : null];
		echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 39 */;
		echo ' type="email" name="email" id="email" placeholder="email@example.com" value="';
		echo LR\Filters::escapeHtmlAttr($email) /* line 39 */;
		echo '" autocomplete="email" required>
            <p class="error" data-errorfield="email">';
		echo LR\Filters::escapeHtmlText($emailError) /* line 40 */;
		echo '</p>
          </div>

          <div class="login-form__form-group">
            <label for="password">Heslo</label>
            <input';
		$ʟ_tmp = ['class' => !empty($passwordError) ? 'has-error' : null];
		echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 45 */;
		echo ' type="password" name="password" id="password" autocomplete="new-password" required>
            <p class="error" data-errorfield="password">';
		echo LR\Filters::escapeHtmlText($passwordError) /* line 46 */;
		echo '</p>
          </div>

          <div class="login-form__form-group">
            <label for="confirm-password">Zopakujte heslo</label>
            <input';
		$ʟ_tmp = ['class' => !empty($confirmPasswordError) ? 'has-error' : null];
		echo Latte\Essential\Nodes\NAttrNode::attrs(isset($ʟ_tmp[0]) && is_array($ʟ_tmp[0]) ? $ʟ_tmp[0] : $ʟ_tmp, false) /* line 51 */;
		echo ' type="password" name="confirmPassword" id="confirm-password" autocomplete="new-password" required>
            <p class="error" data-errorfield="confirmPassword">';
		echo LR\Filters::escapeHtmlText($confirmPasswordError) /* line 52 */;
		echo '</p>
          </div>

          <div class="login-form__form-group">
            <button class="button--fill" type="submit" name="register_btn">Registrovat se</button>
          </div>
        </form>
      </div>

      <div class="login-card__image">
        <img src="assets/img/index.jpg" alt="žena čekající na vlak">
      </div>
    </div>
  </main>
';
	}
}
