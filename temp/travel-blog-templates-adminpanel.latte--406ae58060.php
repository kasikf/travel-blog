<?php

use Latte\Runtime as LR;

/** source: C:\xampp\htdocs\travel-blog/templates/adminpanel.latte */
final class Template406ae58060 extends Latte\Runtime\Template
{
	public const Source = 'C:\\xampp\\htdocs\\travel-blog/templates/adminpanel.latte';

	public const Blocks = [
		['site-title' => 'blockSite_title', 'content' => 'blockContent'],
	];


	public function main(array $ʟ_args): void
	{
		extract($ʟ_args);
		unset($ʟ_args);

		echo "\n";
		$this->renderBlock('site-title', get_defined_vars()) /* line 3 */;
		echo "\n";
		$this->renderBlock('content', get_defined_vars()) /* line 7 */;
	}


	public function prepare(): array
	{
		extract($this->params);

		$this->parentName = ROOT_PATH . '/templates/layouts/layout.latte';
		return get_defined_vars();
	}


	/** {block site-title} on line 3 */
	public function blockSite_title(array $ʟ_args): void
	{
		echo 'TravelBlog - správa
';
	}


	/** {block content} on line 7 */
	public function blockContent(array $ʟ_args): void
	{
		extract($this->params);
		extract($ʟ_args);
		unset($ʟ_args);

		echo '<h1>Vítáme uživatele ';
		echo LR\Filters::escapeHtmlText($_SESSION['user']['User']) /* line 8 */;
		echo '</h1>
';
	}
}
