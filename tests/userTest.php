<?php
require_once(__DIR__ . "/../vendor/autoload.php");
use Tester\Assert;
use TravelBlog\User;
use TravelBlog\Database;

// To make this test pass change the name of the database in src/database.php from travelblog to travelblog_test.

/** @testCase */
class userTest extends \Tester\TestCase {
  public function setUp() {
    Database::setDatabase('travelblog_test');
  }

  public function tearDown() {
    Database::setDatabase('travelblog');
  }
  public function testIsUniqueUser() {
    Assert::true(User::isUniqueUser('Admin698', 'nonexistendadmin@test.org'));
    Assert::false(User::isUniqueUser('Admin', 'admin@test.com'));
  }

  public function testGetUserById() {
    $user = User::getUserById(1);
    Assert::equal('Admin', $user['User']);
    Assert::equal('Admin', $user['UserName']);
    Assert::equal('admin@test.com', $user['UserEmail']);
  }

  public function testRegisterUser() {
    if (User::isUniqueUser('AdminRegister', 'adminregister@test.com')) {
      $userId = User::registerUser('AdminRegister', 'AdminRegister', 'adminregister@test.com', 'admin');
      Assert::notEqual(0, $userId);

      $isLogged = User::loginUser('AdminRegister', 'admin');
      Assert::true($isLogged);
      User::removeUser($userId);
    }
  }
}

(new userTest)->run();