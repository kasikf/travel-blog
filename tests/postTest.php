<?php
use TravelBlog\Database;
require_once(__DIR__ . "/../vendor/autoload.php");
use Tester\Assert;
use TravelBlog\Post;

// To make this test pass change the name of the database in src/database.php from travelblog to travelblog_test.

/** @testCase */
class postTest extends \Tester\TestCase {
  public function setUp() {
    Database::setDatabase('travelblog_test');
  }

  public function tearDown() {
    Database::setDatabase('travelblog');
  }

  public function testCanGetPost() {
    $post = Post::getPost(1);
    Assert::type(Post::class, $post);
    Assert::equal('Test title', $post->getTitle());
    Assert::equal('Test content', $post->getContent());
    Assert::equal('image.jpg', $post->getProfileImg());
    Assert::equal('2024-01-01', $post->getDatePublic());
    Assert::notNull($post->getAuthor());
    Assert::notNull($post->getDestination());
  }

  public function testCanGetAllPosts() {
    $posts = Post::getAllPosts();
    Assert::count(3, $posts);
    Assert::type(Post::class, $posts[0]);
    Assert::equal('Test title', $posts[0]->getTitle());
    Assert::equal('Test content', $posts[0]->getContent());
    Assert::equal('image.jpg', $posts[0]->getProfileImg());
    Assert::equal('2024-01-01', $posts[0]->getDatePublic());
    Assert::notNull($posts[0]->getAuthor());
    Assert::notNull($posts[0]->getDestination());
  }

  public function testCanGetPostDestination() {
    $destination = Post::getPostDestination(1);
    Assert::equal('1', $destination['idDestination']);
    Assert::equal('Destination 1', $destination['Name']);
  }

  public function testCanGetPostAuthor() {
    $author = Post::getPostAuthor(1);
    Assert::equal('1', $author['idUsers']);
    Assert::equal('Admin', $author['UserName']);
    Assert::equal('Admin', $author['User']);
  }

  public function testCanGetDestinations() {
    $destinations = Post::getDestinations();
    Assert::count(3, $destinations);
    Assert::equal('1', $destinations[0]['idDestination']);
    Assert::equal('Destination 1', $destinations[0]['Name']);
  }

  public function testCanGetAuthors() {
    $authors = Post::getAuthors();
    Assert::count(3, $authors);
    Assert::equal('1', $authors[0]['idUsers']);
    Assert::equal('Admin', $authors[0]['UserName']);
    Assert::equal('Admin', $authors[0]['User']);
  }

  public function testCanGetFilteredPosts() {
    $posts = Post::getFilteredPosts();
    Assert::count(3, $posts);
    Assert::type(Post::class, $posts[0]);
    Assert::equal('Test title', $posts[0]->getTitle());
    Assert::equal('Test content', $posts[0]->getContent());
    Assert::equal('image.jpg', $posts[0]->getProfileImg());
    Assert::equal('2024-01-01', $posts[0]->getDatePublic());
    Assert::notNull($posts[0]->getAuthor());
    Assert::notNull($posts[0]->getDestination());

    $posts = Post::getFilteredPosts('1');
    Assert::count(1, $posts);
    Assert::equal('1', $posts[0]->getAuthor()['idUsers']);
    Assert::equal('1', $posts[0]->getDestination()['idDestination']);

    $posts = Post::getFilteredPosts('all', '2');
    Assert::count(1, $posts);
    Assert::equal('2', $posts[0]->getAuthor()['idUsers']);
    Assert::equal('2', $posts[0]->getDestination()['idDestination']);

    $posts = Post::getFilteredPosts('all', 'all', 'test');
    Assert::count(3, $posts);

    $posts = Post::getFilteredPosts('all', 'all', 'Test');
    Assert::count(3, $posts);

    $posts = Post::getFilteredPosts('all', 'all', 'experiment');
    Assert::count(0, $posts);
  }
}

(new postTest)->run();