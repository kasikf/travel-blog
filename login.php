<?php
require_once('config.php');
require_once(ROOT_PATH . "/vendor/autoload.php");

$latte = new Latte\Engine;
$latte->setTempDirectory('temp');

if (isset($_SESSION['user'])) {
  header('Location: ' . BASE_URL . 'adminpanel.php');
}

$errors = array();
$username = '';
$password = '';
$usernameError = '';
$passwordError = '';

if (isset($_POST['login_btn'])) {
  $username = trim($_POST['username']);
  $password = trim($_POST['password']);

  if (empty($username)) { array_push($errors, "Zadejte uživatelské jméno"); $usernameError = "Zadejte uživatelské jméno"; }
  if (empty($password)) { array_push($errors, "Zadejte heslo"); $passwordError = "Zadejte heslo"; }

  if (count($errors) == 0) {
    $user = TravelBlog\User::loginUser($username, $password);
    
    if ($user) {
      header('Location: ' . BASE_URL . '/adminpanel.php');
    } else {
      array_push($errors, "Neplatné uživatelské jméno nebo heslo");
      $passwordError = "Neplatné uživatelské jméno nebo heslo";
    }
  }
}

$params = [
    'usernameError' => $usernameError,
    'passwordError' => $passwordError,
    'username' => $username,
];

$latte->render(ROOT_PATH . '/templates/login.latte', $params);