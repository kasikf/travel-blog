<?php
require_once('config.php');
require_once(ROOT_PATH . "/vendor/autoload.php");

$latte = new Latte\Engine;
$latte->setTempDirectory('temp');

$params = [
    
];

$latte->render(ROOT_PATH . '/templates/adminpanel.latte', $params);