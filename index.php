<?php
require_once('config.php');
require_once(ROOT_PATH . "/vendor/autoload.php");

$latte = new Latte\Engine;
$latte->setTempDirectory('temp');

$posts = TravelBlog\Post::getAllPosts();
$destinations = TravelBlog\Post::getDestinations();

$params = [
    "posts" => $posts,
    "destinations" => $destinations,
];

$latte->render(ROOT_PATH . '/templates/index.latte', $params);