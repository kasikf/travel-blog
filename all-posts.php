<?php
require_once('config.php');
require_once(ROOT_PATH . "/vendor/autoload.php");

$latte = new Latte\Engine;
$latte->setTempDirectory('temp');

$selectedDestination = 'all';
$selectedAuthor = 'all';
$searchTerm = '';
$sortAsc = false;

if (!isset($_GET['reset'])) {
    if (isset($_GET['sortAsc'])) {
        $sortAsc = $_GET['sortAsc'];
    }

    if (isset($_GET['idDestination'])) {
        $selectedDestination = $_GET['idDestination'];
    }

    if (isset($_GET['idUsers'])) {
        $selectedAuthor = $_GET['idUsers'];
    }

    if (isset($_GET['searchTerm'])) {
        $searchTerm = $_GET['searchTerm'];
    }
}


$posts = TravelBlog\Post::getFilteredPosts($selectedDestination, $selectedAuthor, $searchTerm, $sortAsc);
$destinations = TravelBlog\Post::getDestinations();
$authors = TravelBlog\Post::getAuthors();

$params = [
    "posts" => $posts,
    "destinations" => $destinations,
    "authors" => $authors,
    "selectedDestination" => $selectedDestination,
    "selectedAuthor" => $selectedAuthor,
    "searchTerm" => $searchTerm,
    "sortAsc" => $sortAsc,
];

$latte->render(ROOT_PATH . '/templates/all-posts.latte', $params);